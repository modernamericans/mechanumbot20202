
package frc.robot.subsystems;

import frc.robot.Robot;
import frc.robot.commands.*;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PWMVictorSPX;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.drive.MecanumDrive;
import com.revrobotics.*;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;




public class DriveTrain extends Subsystem {

private CANSparkMax speedController1;
private CANSparkMax speedController2; //I don't know at all
private CANSparkMax speedController3;
private CANSparkMax speedController4;
private MecanumDrive mecanumDrive1;


    private final CANSparkMax /*motorLeft1,*/ motorLeft1, motorLeft2;
    private final CANSparkMax /*motorRight1,*/ motorRight1, motorRight2; ///I don't really know how this interacts, but it does
	public final MecanumDrive drive;
	private int direction = DRIVE_TRAIN_FORWARD_DIRECTION;



    public static final int DRIVE_TRAIN_FORWARD_DIRECTION = 1; //I don't know what this does
	public static final int MOTOR_DRIVE_LEFT1 = 1;
	public static final int MOTOR_DRIVE_LEFT2 = 2;
	public static final int MOTOR_DRIVE_RIGHT1 = 3; //just added these for convenience sake, easier to change values
    public static final int MOTOR_DRIVE_RIGHT2 = 4;
    
    public DriveTrain() {
        
        super(); //I don't know what this "super" does and I'm too scared to ask
		motorLeft1 = new CANSparkMax(MOTOR_DRIVE_LEFT1, MotorType.kBrushless); //we are gonna have to figure out the spark max equivalent of this
		motorLeft2 = new CANSparkMax(MOTOR_DRIVE_LEFT2, MotorType.kBrushless);
		motorRight1 = new CANSparkMax(MOTOR_DRIVE_RIGHT1, MotorType.kBrushless);
		motorRight2 = new CANSparkMax(MOTOR_DRIVE_RIGHT2, MotorType.kBrushless);
		SpeedControllerGroup left1 = new SpeedControllerGroup(motorLeft1);//left front
		SpeedControllerGroup left2 = new SpeedControllerGroup(motorLeft2);//left back
		SpeedControllerGroup right1 = new SpeedControllerGroup(motorRight1);//right front
		SpeedControllerGroup right2 = new SpeedControllerGroup(motorRight2);//right back
		
		//mecanum speed command math
		double rf,rb,lf,lb;
		double forward = -Robot.oi.getJoystick().getY();
		double right = Robot.oi.getJoystick().getX();
		double clockwise = Robot.oi.getJoystick().getZ();
		double K = .01;//the value that determines sensitivity of turning tweek to edit
		clockwise = K*clockwise;
		//inverse kinimatics
		rf = forward + clockwise + right;
		lf = forward - clockwise - right;
		lb = forward + clockwise - right;
		rb = forward - clockwise + right;
		left1.set(lf);
		left2.set(lb);
		right1.set(rf);
		right2.set(rb);
		
		drive = new MecanumDrive(left1,left2,right1,right2);

        
/*mecanumDrive1 = new MecanumDrive(speedController1, speedController2,
speedController3, speedController4);
addChild("Mecanum Drive 1",mecanumDrive1);
mecanumDrive1.setSafetyEnabled(true);
mecanumDrive1.setExpiration(0.1);
mecanumDrive1.setMaxOutput(1.0);

 */       
    }

    @Override
    public void initDefaultCommand() {
        
        // Set the default command for a subsystem here.
        // setDefaultCommand(new MySpecialCommand());
    }

    @Override
    public void periodic() {
        // Put code here to be run every loop

    }


    	//what is being seen by the mecanumDriveTrain class 
	public void drive(Joystick joystick) {
		//driveOrig(joystick);
		driveMecanum(joystick);
		
	}
	//what is driving the robot
	public void driveMecanum(Joystick joystick) {
		
		drive.driveCartesian(Robot.oi.stick.getX(), Robot.oi.stick.getY(), Robot.oi.stick.getZ(), 0);
		
		
		
	}

    // Put methods for controlling this subsystem
    // here. Call these from Commands.

}

